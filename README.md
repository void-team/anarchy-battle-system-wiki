# Anarchy Battle System

Anarchy Battle System (AKA Anarchy (Friendly Name), ABS (Internal Name)) is a multi branch battle system that aims to provide fair, balanced battles and rankings. Each branch has it's own special rules and sets, but they all try to measure the player's skill as accurately as possible. Anarchy acts as a physical person in each game, coaching them on their attacks and gameplay.

## Cyberfight Branch

Cyberfight's branch is more tuned for high stakes, high octane battles. It uses 2 catergories to measure the player's skill.

### Offense

Anarchy will measure for each attack it's max damage on a player and it's actual damage. It averages these values together on an attack to attack basis, giving players an insight on what attacks they need to work on mastering.

### Defense

For each hit or barely-missed attack, Anarchy will measure the max damage an attack does to the player, and the actual damage taken by that attack. It averages these values together just like the offense cat, and gives the player feedback on what attacks they need to work on avoiding.

## Card Game Branch

Card Game's branch is the same as Cyberfight's, but with 2 new catergories. 

### Time

Anarchy measures the time it takes for the player to react to each move, and averages them. For specific card plays, Anarchy will see what ones it takes the player longer to react to, and gives the player tips on what they need to work on.

### Reactibility

Anarchy will take the known card plays, and sees how the player reacts to them. It measures the players' reaction to the play on a scale of effectiveness. If their reaction is effective, points are added. If it isn't, Anarchy will note this and train the player to fight against these same plays.

## Origins Branch

Origins' branch is the same as Card Game's branch. That's it. Thought I'd mention that. The only difference is that Anarchy measures skill on a fighter-by-fighter basis, but that's essentally it. 




## Sidenote
You might have picked up on it, but ABS acts as a person. It's a digital coach, designed with improvement in mind, and it encourages the player to get better. 


# Anarchy learns.

Anarchy is capable of learning plays (specific attack combinations) and logs them to the Void Team (or community) server if active/available. The server will save its findings in its storage for the server host to view/share, and gives them suggestions of buffs/debuffs if needed.

# Anarchy (tries to be) Fair.

With Anarchy comes Anarchy Battles. This occurs when players of a certain skill level just aren't available, or a player is too skilled and needs an actual challenge. Instead of normal Free For All or Even-Team matches, Anarchy will bend the teams, for example, a 5v1, 4v2, 3v2v1, 4v1v1, etc. It uses the players' Anarchy levels to figure out how to give the players a tough (but hopefully fair) challenge. If still not enough players are available with this method, Anarchy will add bots with specific Anarchy levels in a last ditch effort to keep things fair.


